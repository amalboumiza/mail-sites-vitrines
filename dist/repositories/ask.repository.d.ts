import { DefaultCrudRepository } from '@loopback/repository';
import { WaialysDataSource } from '../datasources';
import { Ask, AskRelations } from '../models';
export declare class AskRepository extends DefaultCrudRepository<Ask, typeof Ask.prototype.id, AskRelations> {
    constructor(dataSource: WaialysDataSource);
}
