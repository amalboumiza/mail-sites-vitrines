import { DefaultCrudRepository } from '@loopback/repository';
import { WaialysDataSource } from '../datasources';
import { Subscriber, SubscriberRelations } from '../models';
export declare class SubscriberRepository extends DefaultCrudRepository<Subscriber, typeof Subscriber.prototype.id, SubscriberRelations> {
    constructor(dataSource: WaialysDataSource);
}
