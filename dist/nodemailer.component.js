"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodemailerComponent = void 0;
const core_1 = require("@loopback/core");
const keys_1 = require("./keys");
const nodemailer_ask_service_1 = require("./services/nodemailer-ask-service");
// import {NodemailerService} from './services/nodemailer-service';
class NodemailerComponent {
    constructor() {
        this.bindings = [
            core_1.Binding.bind(keys_1.NodemailerBindings.EMAIL_STAG).to(keys_1.NodemailerEmailCredentialsConstants.EMAIL_STAG_CONST),
            core_1.Binding.bind(keys_1.NodemailerBindings.PASS_STAG).to(keys_1.NodemailerEmailCredentialsConstants.PASS_STAG_CONST),
            core_1.Binding.bind(keys_1.NodemailerBindings.EMAIL_PROD).to(keys_1.NodemailerEmailCredentialsConstants.EMAIL_PROD_CONST),
            core_1.Binding.bind(keys_1.NodemailerBindings.PASS_PROD).to(keys_1.NodemailerEmailCredentialsConstants.PASS_PROD_CONST),
            // Binding.bind(NodemailerBindings.SEND_MAIL).toClass(NodemailerService),
            core_1.Binding.bind(keys_1.NodemailerBindings.SEND_MAIL_ASK).toClass(nodemailer_ask_service_1.NodemailerServiceAsk)
        ];
    }
}
exports.NodemailerComponent = NodemailerComponent;
//# sourceMappingURL=nodemailer.component.js.map