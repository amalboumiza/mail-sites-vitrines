import { BindingKey } from '@loopback/context';
import { NodemailerService } from './services/nodemailer-service';
import { NodemailerServiceAsk } from './services/nodemailer-ask-service';
export declare namespace NodemailerBindings {
    const SEND_MAIL: BindingKey<NodemailerService>;
    const SEND_MAIL_ASK: BindingKey<NodemailerServiceAsk>;
    const EMAIL_STAG: BindingKey<string>;
    const PASS_STAG: BindingKey<string>;
    const EMAIL_PROD: BindingKey<string>;
    const PASS_PROD: BindingKey<string>;
}
export declare namespace NodemailerEmailCredentialsConstants {
    const EMAIL_STAG_CONST = "process.env.emailProd";
    const PASS_STAG_CONST = "process.env.passProd";
    const EMAIL_PROD_CONST = "process.env.emailStag";
    const PASS_PROD_CONST = "process.env.passStag";
}
