"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodemailerService = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const dotenv = tslib_1.__importStar(require("dotenv"));
const nodemailer = tslib_1.__importStar(require("nodemailer"));
const keys_1 = require("../keys");
dotenv.config();
let mailObject = {
    to: 'string',
    subject: 'string',
    text: 'string',
    html: 'string',
};
let NodemailerService = class NodemailerService {
    constructor(emailStag, passStag, emailProd, passProd) {
        this.emailStag = emailStag;
        this.passStag = passStag;
        this.emailProd = emailProd;
        this.passProd = passProd;
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            host: 'smtp.gmail.com',
            auth: {
                user: 'tempotimetracking@gmail.com',
                pass: 'tempotimetracking2020',
            },
        });
    }
    sendMail(mailObject) {
        return this.transporter.sendMail(mailObject);
    }
};
NodemailerService = tslib_1.__decorate([
    tslib_1.__param(0, core_1.inject(keys_1.NodemailerBindings.EMAIL_STAG)),
    tslib_1.__param(1, core_1.inject(keys_1.NodemailerBindings.PASS_STAG)),
    tslib_1.__param(2, core_1.inject(keys_1.NodemailerBindings.EMAIL_PROD)),
    tslib_1.__param(3, core_1.inject(keys_1.NodemailerBindings.PASS_PROD)),
    tslib_1.__metadata("design:paramtypes", [String, String, String, String])
], NodemailerService);
exports.NodemailerService = NodemailerService;
//# sourceMappingURL=nodemailer-service.js.map