import * as nodemailer from 'nodemailer';
import { Options } from 'nodemailer/lib/mailer';
export interface NodemailerSericeTypeASK<T = Object> {
    sendMail(mailObject: Options): Promise<T>;
}
export declare class NodemailerServiceAsk {
    private emailStag;
    private passStag;
    private emailProd;
    private passProd;
    private transporter;
    constructor(emailStag: string, passStag: string, emailProd: string, passProd: string);
    sendMail(mailObject: Options): Promise<nodemailer.SentMessageInfo>;
}
