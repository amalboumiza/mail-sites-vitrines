"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SubscriberController = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const keys_1 = require("../keys");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
// import {NodemailerSericeType} from '../services/nodemailer-service';
const path_1 = tslib_1.__importDefault(require("path"));
var loopback = require('loopback');
let SubscriberController = class SubscriberController {
    constructor(subscriberRepository, nodemailer, nodemailerask) {
        this.subscriberRepository = subscriberRepository;
        this.nodemailer = nodemailer;
        this.nodemailerask = nodemailerask;
    }
    async create(subscriber) {
        const addSubscriber = await this.subscriberRepository.create(subscriber);
        var testTemplateMail = loopback.template(path_1.default.resolve(__dirname, '../../templates/welcome.ejs'));
        var myMessage = {
            name: subscriber.name,
            surname: subscriber.surname,
            establishment: subscriber.establishment,
            email: subscriber.email,
            phoneSubscriber: subscriber.phoneSubscriber,
            formationType: subscriber.formationType,
            formationDate: subscriber.formationDate,
        };
        var subscriberAdded = testTemplateMail(myMessage);
        try {
            // sending email to validatos
            await this.nodemailerask.sendMail({
                to: `${subscriber.email}`,
                date: new Date(),
                subject: 'Bienvenue à waialys !',
                html: subscriberAdded
            });
        }
        catch (error) {
            // check if the email is real or not
            console.log(error);
            throw new rest_1.HttpErrors.BadRequest('email invalid ');
        }
        return addSubscriber;
    }
    async count(where) {
        return this.subscriberRepository.count(where);
    }
    async find(filter) {
        return this.subscriberRepository.find(filter);
    }
    async updateAll(subscriber, where) {
        return this.subscriberRepository.updateAll(subscriber, where);
    }
    async findById(id, filter) {
        return this.subscriberRepository.findById(id, filter);
    }
    async updateById(id, subscriber) {
        await this.subscriberRepository.updateById(id, subscriber);
    }
    async replaceById(id, subscriber) {
        await this.subscriberRepository.replaceById(id, subscriber);
    }
    async deleteById(id) {
        await this.subscriberRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    rest_1.post('/subscribers/waialys'),
    rest_1.response(200, {
        description: 'Subscriber model instance',
        content: {
            'application/x-www-form-urlencoded': {
                schema: rest_1.getModelSchemaRef(models_1.Subscriber),
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/x-www-form-urlencoded': {
                schema: rest_1.getModelSchemaRef(models_1.Subscriber, {
                    title: 'NewSubscriber',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "create", null);
tslib_1.__decorate([
    rest_1.get('/subscribers/count'),
    rest_1.response(200, {
        description: 'Subscriber model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.Subscriber)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "count", null);
tslib_1.__decorate([
    rest_1.get('/subscribers'),
    rest_1.response(200, {
        description: 'Array of Subscriber model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.Subscriber, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Subscriber)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "find", null);
tslib_1.__decorate([
    rest_1.patch('/subscribers'),
    rest_1.response(200, {
        description: 'Subscriber PATCH success count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Subscriber, { partial: true }),
            },
        },
    })),
    tslib_1.__param(1, rest_1.param.where(models_1.Subscriber)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Subscriber, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "updateAll", null);
tslib_1.__decorate([
    rest_1.get('/subscribers/{id}'),
    rest_1.response(200, {
        description: 'Subscriber model instance',
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Subscriber, { includeRelations: true }),
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.param.filter(models_1.Subscriber, { exclude: 'where' })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "findById", null);
tslib_1.__decorate([
    rest_1.patch('/subscribers/{id}'),
    rest_1.response(204, {
        description: 'Subscriber PATCH success',
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Subscriber, { partial: true }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, models_1.Subscriber]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "updateById", null);
tslib_1.__decorate([
    rest_1.put('/subscribers/{id}'),
    rest_1.response(204, {
        description: 'Subscriber PUT success',
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.requestBody()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, models_1.Subscriber]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "replaceById", null);
tslib_1.__decorate([
    rest_1.del('/subscribers/{id}'),
    rest_1.response(204, {
        description: 'Subscriber DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", Promise)
], SubscriberController.prototype, "deleteById", null);
SubscriberController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.SubscriberRepository)),
    tslib_1.__param(1, core_1.inject(keys_1.NodemailerBindings.SEND_MAIL)),
    tslib_1.__param(2, core_1.inject(keys_1.NodemailerBindings.SEND_MAIL_ASK)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.SubscriberRepository, Object, Object])
], SubscriberController);
exports.SubscriberController = SubscriberController;
//# sourceMappingURL=subscriber.controller.js.map