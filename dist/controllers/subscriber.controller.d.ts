import { Count, Filter, FilterExcludingWhere, Where } from '@loopback/repository';
import { Subscriber } from '../models';
import { SubscriberRepository } from '../repositories';
import { NodemailerSericeTypeASK } from '../services/nodemailer-ask-service';
import { NodemailerSericeType } from '../services/nodemailer-service';
export declare class SubscriberController {
    subscriberRepository: SubscriberRepository;
    nodemailer: NodemailerSericeType;
    nodemailerask: NodemailerSericeTypeASK;
    constructor(subscriberRepository: SubscriberRepository, nodemailer: NodemailerSericeType, nodemailerask: NodemailerSericeTypeASK);
    create(subscriber: Omit<Subscriber, 'id'>): Promise<Subscriber>;
    count(where?: Where<Subscriber>): Promise<Count>;
    find(filter?: Filter<Subscriber>): Promise<Subscriber[]>;
    updateAll(subscriber: Subscriber, where?: Where<Subscriber>): Promise<Count>;
    findById(id: number, filter?: FilterExcludingWhere<Subscriber>): Promise<Subscriber>;
    updateById(id: number, subscriber: Subscriber): Promise<void>;
    replaceById(id: number, subscriber: Subscriber): Promise<void>;
    deleteById(id: number): Promise<void>;
}
