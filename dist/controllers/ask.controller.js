"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AskController = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let AskController = class AskController {
    constructor(askRepository) {
        this.askRepository = askRepository;
    }
    async create(ask) {
        return this.askRepository.create(ask);
    }
    async count(where) {
        return this.askRepository.count(where);
    }
    async find(filter) {
        return this.askRepository.find(filter);
    }
    async updateAll(ask, where) {
        return this.askRepository.updateAll(ask, where);
    }
    async findById(id, filter) {
        return this.askRepository.findById(id, filter);
    }
    async updateById(id, ask) {
        await this.askRepository.updateById(id, ask);
    }
    async replaceById(id, ask) {
        await this.askRepository.replaceById(id, ask);
    }
    async deleteById(id) {
        await this.askRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    rest_1.post('/asks'),
    rest_1.response(200, {
        description: 'Ask model instance',
        content: { 'application/json': { schema: rest_1.getModelSchemaRef(models_1.Ask) } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Ask, {
                    title: 'NewAsk',
                    exclude: ['id'],
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "create", null);
tslib_1.__decorate([
    rest_1.get('/asks/count'),
    rest_1.response(200, {
        description: 'Ask model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.Ask)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "count", null);
tslib_1.__decorate([
    rest_1.get('/asks'),
    rest_1.response(200, {
        description: 'Array of Ask model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.Ask, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Ask)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "find", null);
tslib_1.__decorate([
    rest_1.patch('/asks'),
    rest_1.response(200, {
        description: 'Ask PATCH success count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Ask, { partial: true }),
            },
        },
    })),
    tslib_1.__param(1, rest_1.param.where(models_1.Ask)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Ask, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "updateAll", null);
tslib_1.__decorate([
    rest_1.get('/asks/{id}'),
    rest_1.response(200, {
        description: 'Ask model instance',
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Ask, { includeRelations: true }),
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.param.filter(models_1.Ask, { exclude: 'where' })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "findById", null);
tslib_1.__decorate([
    rest_1.patch('/asks/{id}'),
    rest_1.response(204, {
        description: 'Ask PATCH success',
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Ask, { partial: true }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, models_1.Ask]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "updateById", null);
tslib_1.__decorate([
    rest_1.put('/asks/{id}'),
    rest_1.response(204, {
        description: 'Ask PUT success',
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__param(1, rest_1.requestBody()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number, models_1.Ask]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "replaceById", null);
tslib_1.__decorate([
    rest_1.del('/asks/{id}'),
    rest_1.response(204, {
        description: 'Ask DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.number('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Number]),
    tslib_1.__metadata("design:returntype", Promise)
], AskController.prototype, "deleteById", null);
AskController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.AskRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.AskRepository])
], AskController);
exports.AskController = AskController;
//# sourceMappingURL=ask.controller.js.map