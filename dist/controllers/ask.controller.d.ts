import { Count, Filter, FilterExcludingWhere, Where } from '@loopback/repository';
import { Ask } from '../models';
import { AskRepository } from '../repositories';
export declare class AskController {
    askRepository: AskRepository;
    constructor(askRepository: AskRepository);
    create(ask: Omit<Ask, 'id'>): Promise<Ask>;
    count(where?: Where<Ask>): Promise<Count>;
    find(filter?: Filter<Ask>): Promise<Ask[]>;
    updateAll(ask: Ask, where?: Where<Ask>): Promise<Count>;
    findById(id: number, filter?: FilterExcludingWhere<Ask>): Promise<Ask>;
    updateById(id: number, ask: Ask): Promise<void>;
    replaceById(id: number, ask: Ask): Promise<void>;
    deleteById(id: number): Promise<void>;
}
