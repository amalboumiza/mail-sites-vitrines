"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WaialysDataSource = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const config = {
    name: 'waialys',
    connector: 'postgresql',
    url: '',
    host: '',
    port: 5432,
    user: 'waialys',
    password: '123456',
    database: 'waialys'
};
// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
let WaialysDataSource = class WaialysDataSource extends repository_1.juggler.DataSource {
    constructor(dsConfig = config) {
        super(dsConfig);
    }
};
WaialysDataSource.dataSourceName = 'waialys';
WaialysDataSource.defaultConfig = config;
WaialysDataSource = tslib_1.__decorate([
    core_1.lifeCycleObserver('datasource'),
    tslib_1.__param(0, core_1.inject('datasources.config.waialys', { optional: true })),
    tslib_1.__metadata("design:paramtypes", [Object])
], WaialysDataSource);
exports.WaialysDataSource = WaialysDataSource;
//# sourceMappingURL=waialys.datasource.js.map