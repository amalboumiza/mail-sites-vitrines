import { Entity } from '@loopback/repository';
export declare class Ask extends Entity {
    id?: number;
    message?: string;
    name?: string;
    email?: string;
    subject?: string;
    constructor(data?: Partial<Ask>);
}
export interface AskRelations {
}
export declare type AskWithRelations = Ask & AskRelations;
