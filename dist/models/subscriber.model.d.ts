import { Entity } from '@loopback/repository';
export declare class Subscriber extends Entity {
    id?: number;
    name?: string;
    surname?: string;
    establishment?: string;
    email?: string;
    phoneSubscriber?: number;
    formationType?: string;
    formationDate?: string;
    constructor(data?: Partial<Subscriber>);
}
export interface SubscriberRelations {
}
export declare type SubscriberWithRelations = Subscriber & SubscriberRelations;
