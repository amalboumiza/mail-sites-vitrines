"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodemailerEmailCredentialsConstants = exports.NodemailerBindings = void 0;
const context_1 = require("@loopback/context");
var NodemailerBindings;
(function (NodemailerBindings) {
    NodemailerBindings.SEND_MAIL = context_1.BindingKey.create('services.nodemailer.sendEmail');
    NodemailerBindings.SEND_MAIL_ASK = context_1.BindingKey.create('services.nodemailer.sendEmail');
    NodemailerBindings.EMAIL_STAG = context_1.BindingKey.create('services.nodemailer.emailStag');
    NodemailerBindings.PASS_STAG = context_1.BindingKey.create('services.nodemailer.passStag');
    NodemailerBindings.EMAIL_PROD = context_1.BindingKey.create('services.nodemailer.emailProd');
    NodemailerBindings.PASS_PROD = context_1.BindingKey.create('services.nodemailer.passProd');
})(NodemailerBindings = exports.NodemailerBindings || (exports.NodemailerBindings = {}));
var NodemailerEmailCredentialsConstants;
(function (NodemailerEmailCredentialsConstants) {
    NodemailerEmailCredentialsConstants.EMAIL_STAG_CONST = 'process.env.emailProd';
    NodemailerEmailCredentialsConstants.PASS_STAG_CONST = 'process.env.passProd';
    NodemailerEmailCredentialsConstants.EMAIL_PROD_CONST = 'process.env.emailStag';
    NodemailerEmailCredentialsConstants.PASS_PROD_CONST = 'process.env.passStag';
})(NodemailerEmailCredentialsConstants = exports.NodemailerEmailCredentialsConstants || (exports.NodemailerEmailCredentialsConstants = {}));
//# sourceMappingURL=keys.js.map