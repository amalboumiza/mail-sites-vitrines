import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {WaialysDataSource} from '../datasources';
import {Ask, AskRelations} from '../models';

export class AskRepository extends DefaultCrudRepository<
  Ask,
  typeof Ask.prototype.id,
  AskRelations
> {
  constructor(
    @inject('datasources.waialys') dataSource: WaialysDataSource,
  ) {
    super(Ask, dataSource);
  }
}
