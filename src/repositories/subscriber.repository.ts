import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {WaialysDataSource} from '../datasources';
import {Subscriber, SubscriberRelations} from '../models';

export class SubscriberRepository extends DefaultCrudRepository<
  Subscriber,
  typeof Subscriber.prototype.id,
  SubscriberRelations
> {
  constructor(
    @inject('datasources.waialys') dataSource: WaialysDataSource,
  ) {
    super(Subscriber, dataSource);
  }
}
