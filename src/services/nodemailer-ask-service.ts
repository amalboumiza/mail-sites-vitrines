import {inject} from '@loopback/core';
import * as dotenv from 'dotenv';
import * as nodemailer from 'nodemailer';
import {Options} from 'nodemailer/lib/mailer';
import {NodemailerBindings} from '../keys';
dotenv.config();

let mailObject = {
  to: 'string',
  subject: 'string',
  text: 'string',
  html: 'string',
};
export interface NodemailerSericeTypeASK<T = Object> {
  sendMail(mailObject: Options): Promise<T>;
}

export class NodemailerServiceAsk {
  private transporter: any;
  constructor(
    @inject(NodemailerBindings.EMAIL_STAG) private emailStag: string,
    @inject(NodemailerBindings.PASS_STAG) private passStag: string,
    @inject(NodemailerBindings.EMAIL_PROD) private emailProd: string,
    @inject(NodemailerBindings.PASS_PROD) private passProd: string,
  ) {
    this.transporter = nodemailer.createTransport({
      service: 'gmail',
      host: 'smtp.gmail.com',
      auth: {user: 'lecadre.adn.expertise@gmail.com', pass: 'lecadre2021'},
    });
  }

  sendMail(mailObject: Options): Promise<nodemailer.SentMessageInfo> {
    return this.transporter.sendMail(mailObject);
  }
}
