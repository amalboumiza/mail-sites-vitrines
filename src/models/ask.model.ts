import {Entity, model, property} from '@loopback/repository';

@model()
export class Ask extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  message?: string;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'string',
  })
  subject?: string;

  constructor(data?: Partial<Ask>) {
    super(data);
  }
}

export interface AskRelations {
  // describe navigational properties here
}

export type AskWithRelations = Ask & AskRelations;
