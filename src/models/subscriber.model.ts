import {Entity, model, property} from '@loopback/repository';

@model()
export class Subscriber extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  surname?: string;

  @property({
    type: 'string',
  })
  establishment?: string;

  @property({
    type: 'string',
  })
  email?: string;

  @property({
    type: 'number',
  })
  phoneSubscriber?: number;

  @property({
    type: 'string',
  })
  formationType?: string;

  @property({
    type: 'string',
  })
  formationDate?: string;

  constructor(data?: Partial<Subscriber>) {
    super(data);
  }
}

export interface SubscriberRelations {
  // describe navigational properties here
}

export type SubscriberWithRelations = Subscriber & SubscriberRelations;
