import {inject} from '@loopback/core';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  HttpErrors,
  param,
  patch,
  post,
  put,
  requestBody,
  response,
} from '@loopback/rest';
import {NodemailerBindings} from '../keys';
import {Subscriber} from '../models';
import {SubscriberRepository} from '../repositories';
// import {NodemailerSericeType} from '../services/nodemailer-service';
import path from 'path';
import {NodemailerSericeTypeASK} from '../services/nodemailer-ask-service';
import {NodemailerSericeType} from '../services/nodemailer-service';
var loopback = require('loopback');




export class SubscriberController {
  constructor(
    @repository(SubscriberRepository)
    public subscriberRepository: SubscriberRepository,
    @inject(NodemailerBindings.SEND_MAIL)
    public nodemailer: NodemailerSericeType,
    @inject(NodemailerBindings.SEND_MAIL_ASK)
    public nodemailerask: NodemailerSericeTypeASK,
  ) {}

  @post('/subscribers/waialys')
  @response(200, {
    description: 'Subscriber model instance',
    content: {
      'application/x-www-form-urlencoded': {
        schema: getModelSchemaRef(Subscriber),
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/x-www-form-urlencoded': {
          schema: getModelSchemaRef(Subscriber, {
            title: 'NewSubscriber',
            exclude: ['id'],
          }),
        },
      },
    })
    subscriber: Omit<Subscriber, 'id'>,
  ): Promise<Subscriber> {
    const addSubscriber = await this.subscriberRepository.create(subscriber);

    var testTemplateMail = loopback.template(
      path.resolve(__dirname, '../../templates/welcome.ejs'),
    );

    var myMessage = {
      name: subscriber.name,
      surname: subscriber.surname,
      establishment: subscriber.establishment,
      email: subscriber.email,
      phoneSubscriber:subscriber.phoneSubscriber,
      formationType: subscriber.formationType,
      formationDate: subscriber.formationDate,
    };

    var subscriberAdded = testTemplateMail(myMessage);



    try {
      // sending email to validatos
      await this.nodemailerask.sendMail({
        to: `${subscriber.email}`,
        date: new Date(),
        subject: 'Bienvenue à waialys !',
        html: subscriberAdded
      });
    } catch (error) {
      // check if the email is real or not
      console.log(error);
      throw new HttpErrors.BadRequest('email invalid ');
    }

    return addSubscriber;
  }

  @get('/subscribers/count')
  @response(200, {
    description: 'Subscriber model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Subscriber) where?: Where<Subscriber>,
  ): Promise<Count> {
    return this.subscriberRepository.count(where);
  }

  @get('/subscribers')
  @response(200, {
    description: 'Array of Subscriber model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Subscriber, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Subscriber) filter?: Filter<Subscriber>,
  ): Promise<Subscriber[]> {
    return this.subscriberRepository.find(filter);
  }

  @patch('/subscribers')
  @response(200, {
    description: 'Subscriber PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscriber, {partial: true}),
        },
      },
    })
    subscriber: Subscriber,
    @param.where(Subscriber) where?: Where<Subscriber>,
  ): Promise<Count> {
    return this.subscriberRepository.updateAll(subscriber, where);
  }

  @get('/subscribers/{id}')
  @response(200, {
    description: 'Subscriber model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Subscriber, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Subscriber, {exclude: 'where'})
    filter?: FilterExcludingWhere<Subscriber>,
  ): Promise<Subscriber> {
    return this.subscriberRepository.findById(id, filter);
  }

  @patch('/subscribers/{id}')
  @response(204, {
    description: 'Subscriber PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Subscriber, {partial: true}),
        },
      },
    })
    subscriber: Subscriber,
  ): Promise<void> {
    await this.subscriberRepository.updateById(id, subscriber);
  }

  @put('/subscribers/{id}')
  @response(204, {
    description: 'Subscriber PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() subscriber: Subscriber,
  ): Promise<void> {
    await this.subscriberRepository.replaceById(id, subscriber);
  }

  @del('/subscribers/{id}')
  @response(204, {
    description: 'Subscriber DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.subscriberRepository.deleteById(id);
  }
}
