import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Ask} from '../models';
import {AskRepository} from '../repositories';

import path from 'path';
import {NodemailerSericeTypeASK} from '../services/nodemailer-ask-service';
var loopback = require('loopback');

export class AskController {
  constructor(
    @repository(AskRepository)
    public askRepository: AskRepository,
  ) {}

  @post('/asks')
  @response(200, {
    description: 'Ask model instance',
    content: {'application/json': {schema: getModelSchemaRef(Ask)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ask, {
            title: 'NewAsk',
            exclude: ['id'],
          }),
        },
      },
    })
    ask: Omit<Ask, 'id'>,
  ): Promise<Ask> {
    return this.askRepository.create(ask);
  }

  @post('/asks/subscribers')
  @response(200, {
    description: 'Ask model instance',
    content: {
      'application/x-www-form-urlencoded': {schema: getModelSchemaRef(Ask)},
    },
  })
  async createSubscriber(
    @requestBody({
      content: {
        'application/x-www-form-urlencoded': {
          schema: getModelSchemaRef(Ask, {
            title: 'NewAsk',
            exclude: ['id'],
          }),
        },
      },
    })
    ask: Omit<Ask, 'id'>,
  ): Promise<Ask> {
    const addSubscriberAsk = await this.askRepository.create(ask);

    var testTemplateMail = loopback.template(
      path.resolve(__dirname, '../../templates/welcome.ejs'),
    );

    var myMessage = {
      message: ask.name,
      name: ask.surname,
      email: ask.establishment,
      subject: subscriber.phoneSubscriber,
    };

    return addSubscriberAsk;
  }

  @get('/asks/count')
  @response(200, {
    description: 'Ask model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(@param.where(Ask) where?: Where<Ask>): Promise<Count> {
    return this.askRepository.count(where);
  }

  @get('/asks')
  @response(200, {
    description: 'Array of Ask model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Ask, {includeRelations: true}),
        },
      },
    },
  })
  async find(@param.filter(Ask) filter?: Filter<Ask>): Promise<Ask[]> {
    return this.askRepository.find(filter);
  }

  @patch('/asks')
  @response(200, {
    description: 'Ask PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ask, {partial: true}),
        },
      },
    })
    ask: Ask,
    @param.where(Ask) where?: Where<Ask>,
  ): Promise<Count> {
    return this.askRepository.updateAll(ask, where);
  }

  @get('/asks/{id}')
  @response(200, {
    description: 'Ask model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Ask, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Ask, {exclude: 'where'}) filter?: FilterExcludingWhere<Ask>,
  ): Promise<Ask> {
    return this.askRepository.findById(id, filter);
  }

  @patch('/asks/{id}')
  @response(204, {
    description: 'Ask PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Ask, {partial: true}),
        },
      },
    })
    ask: Ask,
  ): Promise<void> {
    await this.askRepository.updateById(id, ask);
  }

  @put('/asks/{id}')
  @response(204, {
    description: 'Ask PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() ask: Ask,
  ): Promise<void> {
    await this.askRepository.replaceById(id, ask);
  }

  @del('/asks/{id}')
  @response(204, {
    description: 'Ask DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.askRepository.deleteById(id);
  }
}
