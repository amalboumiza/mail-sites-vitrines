import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'waialys',
  connector: 'postgresql',
  url: '',
  host: '',
  port: 5432,
  user: 'waialys',
  password: '123456',
  database: 'waialys'
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class WaialysDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'waialys';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.waialys', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
